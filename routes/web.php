<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/', 'slot')->name('home');
Route::view('/casino-online', 'casino-online')->name('casino');
Route::view('/judi-bola', 'judi-bola')->name('judi-bola');
Route::view('/judi-online', 'judi-online')->name('judi-online');
